from setuptools import setup

setup(
    name='ds_module',
    version='0.0.2',
    description='My private package from private github repo',
    url='git@gitlab.com:lionel.chamorro/example-ds-modules.git',
    author='Raphael Schubert',
    author_email='lionel.chamorro85@gmail.com',
    license='unlicense',
    packages=['ds_module'],
    zip_safe=False
)
